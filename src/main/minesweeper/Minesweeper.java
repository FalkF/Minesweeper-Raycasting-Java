package main.minesweeper;

import java.util.Random;

public class Minesweeper {
    private final Integer sizeX, sizeY, sizeTotal, mineCount;
    private Cell[][] cell;
    private Integer visibleCount = 0;
    private Integer result = 0;

    public Minesweeper(Integer sizeX, Integer sizeY, Float mineRatio) {
        cell = new Cell[sizeX][sizeY];
        sizeTotal = sizeX * sizeY;
        this.sizeY = sizeY;
        this.sizeX = sizeX;
        mineCount = (int) (sizeTotal * mineRatio);

        for (int x = 0; x < sizeX; x++)
            for (int y = 0; y < sizeY; y++)
                cell[x][y] = new Cell();

        plantMines();
    }

    /**
     * reveals the neigbours of a cell with 0 neighbouring mines
     *
     * @param targetX
     * @param targetY
     */
    private void revealNeighbors(Integer targetX, Integer targetY) {
        for (int x = targetX - 1; x <= targetX + 1; x++) {
            for (int y = targetY - 1; y <= targetY + 1; y++) {
                if (cellExists(x, y) && !cell[x][y].getIsMine() && !cell[x][y].getVisible()) {
                    revealCell(x, y);
                }
            }
        }
    }

    /**
     * sets at least one cell visible
     * win/loose parameters get checked
     * @param x
     * @param y
     */
    public void revealCell(Integer x, Integer y) {
        if (cellExists(x, y) && !cell[x][y].getVisible()) {
            cell[x][y].setVisible();
            visibleCount++;

            if (cell[x][y].getMineCount() == 0) revealNeighbors(x, y);
            if (cell[x][y].getIsMine()) result = -1;
            if (visibleCount == sizeTotal - mineCount) result = 1;
        }
    }

    /**
     * randomly plants mines and increases
     * the mineCount of neighbouring cells
     */
    private void plantMines() {
        Random random = new Random();
        int mineX;
        int mineY;

        for (int i = 0; i < mineCount; i++) {
            do {
                mineX = random.nextInt(sizeX);
                mineY = random.nextInt(sizeY);
            } while (cell[mineX][mineY].getIsMine());

            cell[mineX][mineY].setIsMine();

            // increase mineCount of neighbouring cells
            for (int x = mineX - 1; x <= mineX + 1; x++) {
                for (int y = mineY - 1; y <= mineY + 1; y++) {
                    if (cellExists(x, y) && !cell[x][y].getIsMine()) {
                        cell[x][y].setMineCount();
                    }
                }
            }
        }
    }

    /**
     * returns true if a cell with given coordinates exists
     * @param x
     * @param y
     * @return
     */
    private Boolean cellExists(Integer x, Integer y) {
        return x > -1 && y > -1 && x < sizeX && y < sizeY;
    }

    /**
     * creates a map for the raycasting game
     * the size is double the row/column size + 3,
     * so that there is walkable space around every cell
     */
    public int[][] getRaycastingMap() {
        int mapX = sizeX * 2 + 3;
        int mapY = sizeY * 2 + 3;
        int[][] result = new int[mapX][mapY];

        for (int x = 0; x < mapX; x++) {
            for (int y = 0; y < mapY; y++) {
                if (x == 0 || y == 0 || x == mapX - 1 || y == mapY - 1) {
                    result[x][y] = 4; // greystone
                } else if (x % 2 == 0 && y % 2 == 0) {
                    if (cell[x / 2 - 1][y / 2 - 1].getVisible()) {
                        if (cell[x / 2 - 1][y / 2 - 1].getIsMine()) {
                            result[x][y] = 14; // mine
                        } else {
                            result[x][y] = cell[x / 2 - 1][y / 2 - 1].getMineCount() + 5; // number
                        }
                    } else {
                        result[x][y] = 1; // wood (unrevealed cell)
                    }
                } else {
                    result[x][y] = 0; // free
                }
            }
        }
        return result;
    }

    /**
     * resets the game for replay
     */
    public void reset() {
        for (int x = 0; x < sizeX; x++) {
            for (int y = 0; y < sizeY; y++) {
                cell[x][y].setIsMine(false);
                cell[x][y].setMineCount(0);
                cell[x][y].setVisible(false);
            }
        }

        result = 0;
        visibleCount = 0;

        plantMines();
    }

    public Integer getResult() {
        return result;
    }

    public void print() {
        print(true);
    }

    /**
     * debug function
     * @param onlyVisible true - only revealed cells will be printed
     *                    false - all cells get printed
     */
    public void print(boolean onlyVisible) {
        for (Cell[] x : cell) {
            for (Cell y : x) {
                if (y.getVisible() || !onlyVisible) {
                    if (y.getIsMine()) {
                        System.out.print("* ");
                    } else {
                        System.out.print(y.getMineCount() + " ");
                    }
                } else {
                    System.out.print("? ");
                }
            }
            System.out.println();
        }
    }
}
