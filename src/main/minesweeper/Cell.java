package main.minesweeper;

public class Cell {
    private Integer mineCount = 0;
    private Boolean isMine = false;
    private Boolean visible = false;

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible() {
        visible = true;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Boolean getIsMine() {
        return isMine;
    }

    public void setIsMine() {
        isMine = true;
    }

    public void setIsMine(Boolean isMine) {
        this.isMine = isMine;
    }

    public Integer getMineCount() {
        return mineCount;
    }

    public void setMineCount() {
        mineCount++;
    }

    public void setMineCount(Integer mineCount) {
        this.mineCount = mineCount;
    }
}
