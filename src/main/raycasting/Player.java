package main.raycasting;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;


public class Player implements KeyListener {
    private final double MOVEMENT_SPEED = .08;
    private final double ROTATION_SPEED = .06;
    public double xPos, yPos, xDir, yDir, xPlane, yPlane;
    public boolean selectCell = false;
    private boolean left, right, forward, back;

    public Player(double xPos, double yPos, double xDir, double yDir, double xPlane, double yPlane) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.xDir = xDir;
        this.yDir = yDir;
        this.xPlane = xPlane;
        this.yPlane = yPlane;
    }

    public void keyPressed(KeyEvent key) {
        switch (key.getKeyCode()) {
            case KeyEvent.VK_A:
            case KeyEvent.VK_LEFT:
                left = true;
                break;
            case KeyEvent.VK_D:
            case KeyEvent.VK_RIGHT:
                right = true;
                break;
            case KeyEvent.VK_W:
            case KeyEvent.VK_UP:
                forward = true;
                break;
            case KeyEvent.VK_S:
            case KeyEvent.VK_DOWN:
                back = true;
                break;
            case KeyEvent.VK_SPACE:
                selectCell = true;
                break;
        }
    }

    public void keyReleased(KeyEvent key) {
        switch (key.getKeyCode()) {
            case KeyEvent.VK_A:
            case KeyEvent.VK_LEFT:
                left = false;
                break;
            case KeyEvent.VK_D:
            case KeyEvent.VK_RIGHT:
                right = false;
                break;
            case KeyEvent.VK_W:
            case KeyEvent.VK_UP:
                forward = false;
                break;
            case KeyEvent.VK_S:
            case KeyEvent.VK_DOWN:
                back = false;
                break;
        }
    }

    /**
     * calculate the player position
     *
     * @param map
     */
    public void update(int[][] map) {
        if (forward) {
            if (map[(int) (xPos + xDir * MOVEMENT_SPEED)][(int) yPos] == 0) {
                xPos += xDir * MOVEMENT_SPEED;
            }

            if (map[(int) xPos][(int) (yPos + yDir * MOVEMENT_SPEED)] == 0) {
                yPos += yDir * MOVEMENT_SPEED;
            }
        }

        if (back) {
            if (map[(int) (xPos - xDir * MOVEMENT_SPEED)][(int) yPos] == 0) {
                xPos -= xDir * MOVEMENT_SPEED;
            }

            if (map[(int) xPos][(int) (yPos - yDir * MOVEMENT_SPEED)] == 0) {
                yPos -= yDir * MOVEMENT_SPEED;
            }
        }

        if (right) {
            double oldxDir = xDir;
            xDir = xDir * Math.cos(-ROTATION_SPEED) - yDir * Math.sin(-ROTATION_SPEED);
            yDir = oldxDir * Math.sin(-ROTATION_SPEED) + yDir * Math.cos(-ROTATION_SPEED);

            double oldxPlane = xPlane;
            xPlane = xPlane * Math.cos(-ROTATION_SPEED) - yPlane * Math.sin(-ROTATION_SPEED);
            yPlane = oldxPlane * Math.sin(-ROTATION_SPEED) + yPlane * Math.cos(-ROTATION_SPEED);
        }

        if (left) {
            double oldxDir = xDir;
            xDir = xDir * Math.cos(ROTATION_SPEED) - yDir * Math.sin(ROTATION_SPEED);
            yDir = oldxDir * Math.sin(ROTATION_SPEED) + yDir * Math.cos(ROTATION_SPEED);

            double oldxPlane = xPlane;
            xPlane = xPlane * Math.cos(ROTATION_SPEED) - yPlane * Math.sin(ROTATION_SPEED);
            yPlane = oldxPlane * Math.sin(ROTATION_SPEED) + yPlane * Math.cos(ROTATION_SPEED);
        }
    }

    public void setSelectCell(boolean selectCell) {
        this.selectCell = selectCell;
    }

    public void keyTyped(KeyEvent arg0) {
        // TODO Auto-generated method stub

    }
}
