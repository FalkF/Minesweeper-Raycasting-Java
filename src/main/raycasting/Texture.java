package main.raycasting;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;

public class Texture {

    private final int SIZE;
    private final int[] PIXELS;
    private final String NAME;

    private static final ClassLoader classLoader = Texture.class.getClassLoader();
    private static final String[] TEXTURE_NAMES = {
        "wood",
        "redbrick",
        "bluestone",
        "greystone",
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "mine"
    };

    public Texture(String name, int size) {
        this.NAME = name;
        this.SIZE = size;
        this.PIXELS = new int[SIZE * SIZE];
        load();
    }

    /**
     * loads a single texture
     */
    private void load() {
        try {
            BufferedImage image = ImageIO.read(classLoader.getResourceAsStream("main/res/" + NAME + ".png"));
            int w = image.getWidth();
            int h = image.getHeight();
            image.getRGB(0, 0, w, h, PIXELS, 0, w);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getSIZE() {
        return SIZE;
    }

    public int[] getPIXELS() {
        return PIXELS;
    }

    /**
     * returns an ArrayList containing all Textures
     * used for loading once
     *
     * @return ArrayList<Texture>
     */
    public static ArrayList<Texture> getAll() {
        ArrayList<Texture> textures = new ArrayList<>();

        for (String name : TEXTURE_NAMES) {
            textures.add(new Texture(name, 64));
        }

        return textures;
    }
}
